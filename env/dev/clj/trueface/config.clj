(ns trueface.config
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [trueface.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[trueface started successfully using the development profile]=-"))
   :middleware wrap-dev})
