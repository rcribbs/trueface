(ns trueface.config
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[trueface started successfully]=-"))
   :middleware identity})
