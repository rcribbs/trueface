CREATE TABLE posts
(id SERIAL PRIMARY KEY,
 subject VARCHAR(78),
 content TEXT);
