CREATE TABLE tags_posts
(tag_id INT references tags(id),
 post_id INT references posts(id),
 PRIMARY KEY(tag_id, post_id));
