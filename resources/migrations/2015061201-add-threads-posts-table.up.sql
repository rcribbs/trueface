CREATE TABLE threads_posts
(thread_id INT references threads(id),
 post_id INT references posts(id),
 PRIMARY KEY(thread_id, post_id));
