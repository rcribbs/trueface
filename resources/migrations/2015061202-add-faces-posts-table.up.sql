CREATE TABLE faces_posts
(face_id CHAR(128) references faces(identifier),
 post_id INT references posts(id),
 PRIMARY KEY(face_id, post_id));
