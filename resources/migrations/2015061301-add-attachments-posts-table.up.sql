CREATE TABLE attachments
(filename VARCHAR(260),
 data_type VARCHAR(128),
 encrypted boolean,
 added TIMESTAMP WITH TIME ZONE,
 post_id INT references posts(id) PRIMARY KEY);
