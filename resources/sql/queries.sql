
--name: create-face!
-- creates a new face record
INSERT INTO faces
(identifier, name, born)
VALUES (:identifier, :name, now())

-- name: get-face
-- retrieve a face given the id
SELECT * FROM faces
WHERE identifier = :identifier

-- name: get-face-by-name
-- retrieve a face given the name
SELECT * FROM faces
WHERE name = :name

-- name: tag-post! 
-- create a tag-post relation
INSERT INTO tags_posts
(tag_id, post_id)
VALUES (:tag_id, :post_id)

-- name: create-tag<!
-- create a new tag record 
INSERT INTO tags
(name)
VALUES (:name)

-- name: create-post<!
-- create a new post record 
INSERT INTO posts
(subject, content)
VALUES (:subject, :content)

-- name: get-post
-- retrieve a post given the id
SELECT * FROM posts
WHERE id = :id

-- name: get-tag
-- retrieve a tag given the id
SELECT * FROM tags
WHERE id = :id

-- name: get-tags
-- retrieve all tags 
SELECT * FROM tags

-- name: get-tags-for-post
-- retrieve a list of tags associated with a post 
SELECT t.* FROM tags_posts tp INNER JOIN tags t on t.id = tp.tag_id
WHERE tp.post_id = :post_id

-- name: get-tag-by-name
-- retrieve a tag given the name
SELECT * FROM tags
WHERE name = :name

-- name: get-posts-for-tag 
-- retrieve a list of posts associated with a tag 
SELECT p.* FROM tags_posts tp INNER JOIN posts p on p.id = tp.post_id INNER JOIN tags t on t.id = tp.tag_id
WHERE t.name = :name

-- name: attach-to-post!
-- attach an attachement to post
INSERT INTO attachments
(filename, data_type, encrypted, added, post_id)
VALUES (:filename, :data_type, :encrypted, now(), :post_id)

-- name: get-attachment-for-post
-- retrieve an attachment associated with a post
SELECT * FROM attachments 
WHERE post_id = :post_id

-- name: create-thread<!
-- create a new thread record 
INSERT INTO threads
(first_post_id)
VALUES (:post_id)

-- name: get-posts-for-thread
-- retrieve a list of posts associated with a thread
SELECT p.* FROM posts p INNER JOIN threads t on p.thread_id = t.id

-- name: get-first-post
-- retrieve the first post from a thread
SELECT p.* FROM posts p INNER JOIN threads t on p.id = t.first_post_id
