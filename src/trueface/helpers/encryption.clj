;; Thanks to:
;; http://stackoverflow.com/questions/10221257/is-there-an-aes-library-for-clojure
;; and
;; https://github.com/clavoie/lock-key/blob/master/src/lock_key/core.clj

(ns trueface.helpers.encryption 
  "Wrapper around the Java cryptography extensions in javax.crypto. Implements
  symmetric encryption in AES/CBC/PKCS5Padding mode."
  (:import
        [javax.crypto Cipher KeyGenerator SecretKey]
        [javax.crypto.spec SecretKeySpec]
        [java.security SecureRandom]
        [org.apache.commons.codec.binary Base64]))

(defn utf8-bytes [s]
  (.getBytes s "UTF-8"))

(defn base64 [b]
  (Base64/encodeBase64String b))

(defn debase64 [s]
  (Base64/decodeBase64 (utf8-bytes s)))

(defn get-raw-key [seed]
  (let [keygen (KeyGenerator/getInstance "AES")
        sr (SecureRandom/getInstance "SHA1PRNG")]
    (.setSeed sr (utf8-bytes seed))
    (.init keygen 128 sr)
    (.. keygen generateKey getEncoded)))

(defn get-cipher [mode seed]
  (let [key-spec (SecretKeySpec. (get-raw-key seed) "AES")
        cipher (Cipher/getInstance "AES")]
    (.init cipher mode key-spec)
    cipher))

(defn encrypt [text key]
  (let [utf8-bytes (utf8-bytes text)
        cipher (get-cipher Cipher/ENCRYPT_MODE key)]
    (base64 (.doFinal cipher utf8-bytes))))

(defn decrypt [text key]
  (let [cipher (get-cipher Cipher/DECRYPT_MODE key)]
    (String. (.doFinal cipher (debase64 text)))))
