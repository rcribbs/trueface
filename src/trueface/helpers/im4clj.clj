(ns trueface.helpers.im4clj
  "Wrapper around the im4java imagemagick wrapper." 
  (:import
        [org.im4java.core ConvertCmd IMOperation]))

(defn resize-to-box [image dimension new-file]
  (let [cmd (new ConvertCmd)
        op (new IMOperation)]
    (.addImage op (into-array [image]))
    (.thumbnail op (int dimension) (int dimension) ">")
    (.addImage op (into-array [new-file]))
    (.run cmd op (into-array []))))
