(ns trueface.helpers.tag
  "Helpers for dealing with tags." 
  (:require
        [taoensso.timbre :as timbre :refer  [debug]]
        [trueface.db.core :refer [get-tags-for-post get-attachment-for-post]]
        [trueface.helpers.thumbnail :refer [get-thumb]]
  )
)

(defn add-tags-to-post [posts]
  (defn mapper [post]
    (let [post-id (int (get post :id))
          tags (get-tags-for-post {:post_id post-id})
          tag-names (map #(get % :name) tags)
         ]
      (assoc post :tags tag-names)))
    (map mapper posts))

(defn add-thumbs-to-posts [posts]
  (defn mapper [post]
    (let [filename (get (first (get-attachment-for-post {:post_id (get post :id)})) :filename)
          thumb-info (get-thumb filename)
          thumb-filename (get thumb-info :relative-path)]
      (assoc post :thumb thumb-filename)))
    (map mapper posts))
