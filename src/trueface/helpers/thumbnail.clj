(ns trueface.helpers.thumbnail
  "Helpers for dealing with thumbnails." 
  (:import
        [org.apache.commons.io IOUtils]
  )
  (:require
        [trueface.helpers.im4clj :as im]
        [taoensso.timbre :as timbre :refer  [debug]]
  )
)

(defn thumbnail-with-imagemagick [image-file]
  (let [split-file-path (first (re-seq #"(.*)(\.\w*)$" image-file))
        file-prefix (nth split-file-path 1)
        file-suffix (nth split-file-path 2)
        thumbnail-path (str file-prefix "th" file-suffix)]
    (im/resize-to-box image-file 250 thumbnail-path)
    (IOUtils/toByteArray (new java.io.FileInputStream thumbnail-path))))

(defn get-thumb [filename]
  (let [split-file-path (first (re-seq #"(.*)(\.\w*)$" filename))
        file-prefix (nth split-file-path 1)
        file-suffix (nth split-file-path 2)
        thumbnail-name (str file-prefix "th" file-suffix)
        thumbnail-relative-path (str "/uploaded/" thumbnail-name)
        thumbnail-path (str "./resources/public/uploaded/" thumbnail-name)]
    {:full-path thumbnail-path
     :relative-path thumbnail-relative-path
     :filename thumbnail-name}
    )
  )
