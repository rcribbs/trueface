(ns trueface.routes
  (:require [compojure.core :refer [defroutes GET POST]]
            [trueface.routes.home :refer :all]
            [trueface.routes.post :refer :all]
            [trueface.routes.signup :refer :all]
            [trueface.routes.login :refer :all]
            [trueface.routes.tag :refer :all]
            [trueface.routes.tag-auth :refer :all]
            [trueface.routes.user :refer :all]
            [trueface.routes.debug :refer :all]
  )
)

(defroutes all-routes
  (GET "/" [] (home-page))
  (GET "/about" [] (about-page))

  (GET "/see/:name" [name] (see-name-page name))

  (GET "/tag" [] (list-tags))
  (GET "/tag/:name" [name] (see-tag name))
  (GET "/tag/id/:id" [id] (see-tag-by-id id))

  (GET "/auth" [] (verify-auth))
  (POST "/auth" [password tag_id :as req] (store-auth! password tag_id req))

  (GET "/login" [] (login-page))

  (GET "/signup" [] (signup-page))
  (POST "/signup" [name pass] (process-signup name pass))

  (POST "/post" [subject content tags uploaded-file] (make-post subject content tags uploaded-file))
  (GET "/post" [] (create-post))
  (GET "/post/:id" [id] (view-post id))
  
  (POST "/checkauth" [pass tag_id :as req] (decrypt-auth tag_id pass req))
  (GET "/checkauth/:id" [id :as req] (check-auth id req))
  (GET "/clearauth" req (clear-auth! req))
)
