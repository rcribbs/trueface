(ns trueface.routes.debug
  "Debug routes, certainly not for production."
  (:require [selmer.parser :refer [render-file add-tag!]]
            [ring.util.anti-forgery :refer  [anti-forgery-field]]
            [ring.util.response :refer [redirect response]]
            [taoensso.timbre :as timbre :refer  [info debug]]
            [trueface.db.core :refer :all]
            [trueface.helpers.encryption :as crypt]
  )
)

(defn check-auth [id {session :session}]
  (add-tag! :csrf-token  (fn  [_ _]  (anti-forgery-field)))
  (render-file "check_auth.html" {:tag-id id}))

(defn clear-auth! [{session :session}]
  (->
    (response "")
    (assoc :session (dissoc session :pwid)))
  (redirect "/checkauth"))

(defn decrypt-auth [tag-id password {session :session}]
  (debug tag-id)
  (let [decrypted-password (crypt/decrypt password (get session :pwid))]
    (render-file "show_auth.html" {:tag-id tag-id :password decrypted-password})))
