(ns trueface.routes.home
  (:require [trueface.layout :as layout]
            [trueface.db.core :refer :all]
            [trueface.helpers.encryption :as crypt]
            [trueface.helpers.im4clj :as im]
            [trueface.helpers.thumbnail :refer :all]
            [trueface.routes.post :refer :all]
            [trueface.routes.signup :refer :all]
            [trueface.routes.login :refer :all]
            [trueface.routes.tag :refer :all]
            [trueface.routes.tag-auth :refer :all]
            [clojure.java.io :as io]
            [compojure.core :refer [defroutes GET POST]]
            [ring.util.response :refer [redirect response]]
            [ring.util.http-response :refer :all]
            [ring.util.anti-forgery :refer  [anti-forgery-field]]
            [taoensso.timbre :as timbre :refer  [log info debug]]
            [selmer.parser :refer [render-file add-tag!]]
            [pandect.algo.md5 :refer :all]
            [clj-time.core :as t]
            [clj-time.coerce :as c]
            [clj-time.format :as f]
            [image-resizer.core :refer :all]
  )
)

(defn home-page []
  (layout/render
    "home.html" {:docs (-> "docs/docs.md" io/resource slurp)}))

(defn about-page []
  (layout/render "about.html"))
