(ns trueface.routes.login
  "Route methods for operations having to do with login."
  (:require [selmer.parser :refer [render-file add-tag!]]
            [ring.util.anti-forgery :refer  [anti-forgery-field]]
  )
)

(defn login-page [] 
  (add-tag! :csrf-token  (fn  [_ _]  (anti-forgery-field)))
  (render-file "login.html" {}))
