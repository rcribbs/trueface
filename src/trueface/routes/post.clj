(ns trueface.routes.post
  "Route methods for operations having to do with posts."
  (:require [selmer.parser :refer [render-file add-tag!]]
            [ring.util.anti-forgery :refer  [anti-forgery-field]]
            [clojure.string :refer [split]]
	        [ring.util.response :refer [redirect-after-post]]
            [taoensso.timbre :as timbre :refer  [info debug]]
            [trueface.routes.tag :refer [get-or-create-tag]]
            [trueface.db.core :refer :all]
            [trueface.helpers.thumbnail :refer [thumbnail-with-imagemagick get-thumb]]
            [pandect.algo.md5 :refer [md5-file]]
  )
)

(defn create-post []
  (add-tag! :csrf-token  (fn  [_ _]  (anti-forgery-field)))
  (render-file "post.html" {}))

(defn make-post [subject content tags uploaded-file]
  (debug (str "Creating post with subject: " subject " content: " content " tags: " tags))
  (let [post (create-post<! {:subject subject
                 :content content})]
    (debug (str "Created post: " post))
    (let [list-of-tags (split tags #"\s*[,]{1}\s*")]
      (doseq [tag list-of-tags]
        (debug (str "Searching for tag: " tag)) 
        (let [existing-tag (get-or-create-tag tag)]
          (tag-post! {:tag_id (get existing-tag :id)
                      :post_id (get post :id)})))
      (debug (str "Uploaded file is: " uploaded-file))
      (let [image-folder "./resources/public/uploaded/"
            file-extension (nth (split (get uploaded-file :filename) #"\." 2) 1)
            filename (str (md5-file (get uploaded-file :tempfile)) "." file-extension)
            new-file-path (str image-folder (get uploaded-file :filename))]
        (clojure.java.io/copy (get uploaded-file :tempfile) (new java.io.File new-file-path))
        (attach-to-post! {:filename (get uploaded-file :filename)
                          :data_type (get uploaded-file :content-type)
                          :encrypted false
                          :post_id (get post :id)})
        (thumbnail-with-imagemagick new-file-path))

        (info (str "redirecting to " "/post/" (get post :id)))
        (redirect-after-post (str "/post/" (get post :id))))))

(defn view-post [id]
  (let [post-id (Integer/parseInt id)
        post (get-post {:id post-id})
        attachment (first (get-attachment-for-post {:post_id post-id}))
        tags (get-tags-for-post {:post_id post-id})
        tag-names (map #(get % :name) tags)
        thumb-info (get-thumb (get attachment :filename))
        thumb-path (get thumb-info :relative-path)]
    (debug (pr-str "This post has the following tags: " tag-names))
    (debug attachment)
    (render-file "view_post.html" {:post (first post)
                                   :thumbnail thumb-path
                                   :tags tag-names
                                   :file_link (str "/uploaded/" (get attachment :filename))})))
