(ns trueface.routes.signup
  "Route methods for operations having to do with signup."
  (:require [selmer.parser :refer [render-file add-tag!]]
            [ring.util.anti-forgery :refer  [anti-forgery-field]]
            [taoensso.timbre :as timbre :refer  [info debug]]
            [pandect.algo.sha3-512 :refer :all]
            [trueface.db.core :refer [create-face!]]
  )
)

(defn signup-page []
  (add-tag! :csrf-token  (fn  [_ _]  (anti-forgery-field)))
  (render-file "signup.html" {}))

(defn process-signup [name pass]
  (def identifier (sha3-512 (str pass
                                 (sha3-512 name))))
  (declare fail-string)
  (try (create-face! {:identifier identifier
                      :name name})
       (catch Exception e
        (def error-code (.getSQLState (.getNextException e)))
        (debug (str "Encountered error code: " error-code))
        (if (= error-code "23505") (def fail-string "Duplicate user"))))
  (let [failure fail-string]
    (def fail-string nil)
    (if failure
      (render-file "signup_failure.html" {:failure failure})
      (render-file "signup_finish.html" {}))))
