(ns trueface.routes.tag
  "Route methods for operations having to do with tags."
  (:require [selmer.parser :refer [render-file add-tag!]]
            [ring.util.anti-forgery :refer  [anti-forgery-field]]
            [trueface.db.core :refer [get-tag-by-name create-tag<! get-tags get-posts-for-tag get-tag]]
            [trueface.helpers.tag :refer :all]
            [taoensso.timbre :as timbre :refer  [debug info]]
	        [ring.util.response :refer [redirect]]
  )
)

(defn get-or-create-tag [tag-name]
  (let [existing-tag (first (get-tag-by-name {:name tag-name}))]
    (if (nil? existing-tag)
      (do 
        (debug "No tag exists. Creating tag.")
        (create-tag<! {:name tag-name}))
      (do
        (debug (str "Tag exists: " existing-tag))
        existing-tag))))

(defn list-tags []
  (info "Listing tags.")
  (let [tags (get-tags)]
    (render-file "list_tags.html" {:tags tags})))

(defn see-tag [name]
  (info (str "Looking up posts for tag: " name))
  (let [tag (first (get-tag-by-name {:name name}))
        posts (add-tags-to-post (add-thumbs-to-posts (get-posts-for-tag {:name name})))]
    (debug (str "Found " (count posts) " posts for tag '" name "'.")) 
    (render-file "view_tag.html" {:posts posts})))

(defn see-tag-by-id [id]
  (let [tag (first (get-tag {:id (Integer/parseInt id)}))]
    (redirect (str "/tag/" (get tag :name)))))
