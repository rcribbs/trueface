(ns trueface.routes.tag-auth
  "Route methods for operations having to do with tag authentication."
  (:require [selmer.parser :refer [render-file add-tag!]]
            [ring.util.anti-forgery :refer  [anti-forgery-field]]
            [trueface.helpers.encryption :as crypt]
            [ring.util.http-response :refer [ok header]]
            [digest :refer [sha-512]]
            [clj-time.coerce :refer [to-long]]
            [clj-time.core :refer [now]]
  )
)

(defn verify-auth []
  (add-tag! :csrf-token  (fn  [_ _]  (anti-forgery-field)))
  (render-file "tag_auth.html" {:tag-id "4"}))

(defn store-auth! [password tag-id {session :session}]
  (let [pwkey (sha-512 (str (to-long (now))))]
    (->
      (render-file "store_auth.html" {:session tag-id :password (crypt/encrypt password pwkey)})
      ok
      (header "Content-Type" "text/html")
      (assoc :session (assoc session :pwid pwkey)) 
    )))
