(ns trueface.routes.user
  "Route methods for operations having to do with users."
  (:require [selmer.parser :refer [render-file]]
            [clj-time.coerce :as c]
            [trueface.db.core :refer [get-face-by-name]]
  )
)

(defn see-name-page [name]
  (let [faces (get-face-by-name {:name name})]
    (let [birth-date (get (first faces) :born)]
      (render-file "see_face.html" {:face (first faces)
                                    :relative-birth (c/from-sql-date birth-date)}))))
